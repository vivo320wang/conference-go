from django.db import DEFAULT_DB_ALIAS
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return content["photos"][0]["src"]["original"]

    except:
        return None


city = "Dallas"
state = "TX"


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except:

        return None

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = json.loads(response.content)

    try:
        temp = content["main"]["temp"]
        # print(temp)
        description = content["weather"][0]["description"]
        weather = {"temp": temp, "description": description}

        return weather

    except:
        return None


# print(get_weather_data(city, state))
# {
#     "coord": {"lon": -96.796, "lat": 32.781},
#     "weather": [
#         {
#             "id": 801,
#             "main": "Clouds",
#             "description": "few clouds",
#             "icon": "02d",
#         }
#     ],
#     "base": "stations",
#     "main": {
#         "temp": 306.69,
#         "feels_like": 309.36,
#         "temp_min": 304.96,
#         "temp_max": 308.46,
#         "pressure": 1004,
#         "humidity": 46,
#     },
#     "visibility": 10000,
#     "wind": {"speed": 10.8, "deg": 170, "gust": 16.98},
#     "clouds": {"all": 20},
#     "dt": 1653856027,
#     "sys": {
#         "type": 2,
#         "id": 2018848,
#         "country": "US",
#         "sunrise": 1653823267,
#         "sunset": 1653874106,
#     },
#     "timezone": -18000,
#     "id": 4684888,
#     "name": "Dallas",
#     "cod": 200,
# }
